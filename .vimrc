set cursorline
set number
set ruler
syntax on

" Built-in vim color schemes that are dark-mode compatible:
" delek - too much red
" desert
" elflord
" koehler - comments are too bright
" pablo - comments are too dark on a dark background
" peachpuff
" ron - comments are too bright
" slate
" zellner - too much red
"
" Other schemes looked poor on a dark background
" Listed schemes without comments had poor vimdiff colors

colorscheme apprentice
