echo "Configuring MacOS..."
if [[ "$(defaults read com.apple.dock persistent-apps | wc -l)" -gt "2" ]]; then
  echo "  Removing apps from Dock..."
  defaults write com.apple.dock persistent-apps -array
  killall Dock
fi;
echo "  Configuring key bindings..."
mkdir -p ~/Library/KeyBinding
cp DefaultKeyBinding.dict ~/Library/KeyBinding
echo "  Enabling tap-to-click..."
defaults write com.apple.AppleMultitouchTrackpad Click -bool true
sudo defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Click -bool true
sudo defaults -currentHost write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
sudo defaults write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
echo "Done configuring MacOS"
