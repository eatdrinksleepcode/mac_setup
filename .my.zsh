# This file is copied from the setup repo and sourced by .zshrc
# Installers will sometimes write to .zshrc, so we don't want to overwrite that during setup

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# ZSH wants the group owner for completions to only have read access; but I want all brew users to be able to manage brew
ZSH_DISABLE_COMPFIX="true"

ZSH_THEME="powerlevel10k/powerlevel10k"

DEFAULT_USER="$(whoami)"

plugins=(dotnet git gitfast docker docker-compose)

source $ZSH/oh-my-zsh.sh

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Macs come installed with an older version of curl
# Specifically, one that does not support --output-dir
export PATH="/usr/local/opt/curl/bin:$PATH"

export PATH="$PATH:~/.dotnet/tools"

clean_dotnet() {
  (find . -name bin -type d | xargs -I {} rm -r {}) && (find . -name obj -type d | xargs -I {} rm -r {})
}

rep() {
  COUNT=$1
  shift
  for i in {1..$COUNT}
  do
    "$@"
  done
}

function gps () {
    ps aux | grep "$@" | grep -v grep
}
