# run this script by pasting this command into the terminal:
# /bin/bash -c "$(curl -fsSL https://gitlab.com/eatdrinksleepcode/mac_setup/-/raw/master/clone-and-setup.sh)"

set -euo pipefail

git clone https://gitlab.com/eatdrinksleepcode/mac_setup.git
cd mac_setup
./mac-setup.sh
